# Authors

Currently maintained by **Philipp Zschumme**, **Nico Brandt** and **Ephraim
Schoof**.

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Philipp Zschumme**
* **Nico Brandt**
* **Ephraim Schoof**
* **Patrick Altschuh**
* **Raphael Schoof**
* **Lars Griem**
* **Christoph Herrmann**
* **Zihan Zhang**
* **Julian Grolig**
* **Arnd Koeppe**
