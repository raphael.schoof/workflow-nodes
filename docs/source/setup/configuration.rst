.. _installation-configuration:

Configuration
=============

Most of the tools within the workflow-nodes can be used directly after installation. In
case you are using tools which interact with a Kadi4Mat instance, make sure the
connection is already configured. See
`here <https://kadi-apy.readthedocs.io/en/stable/setup/configuration.html>`__ for
more information.
