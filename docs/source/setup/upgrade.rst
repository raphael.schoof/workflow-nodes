.. _setup-upgrade:

Upgrade
=======

To upgrade, run

.. code-block:: shell

 $ pip3 install --upgrade workflow-nodes

if you installed the library directly without source code. Otherwise fetch the latest
code and install it again.
