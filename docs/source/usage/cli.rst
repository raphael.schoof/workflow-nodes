.. _usage-cli:

CLI
===

The workflow-nodes are created to be used within a workflow editor, however all tools
can also be used directly in the terminal, inside a shell script or by running it as
an external command inside most programming languages. The first entry point to the CLI
is given by running:

.. code-block:: shell

    $ workflow-nodes

All commands concerning different type of tools are available as various subcommands.
For example, all subcommands whcih can be used for plotting are listed by running

.. code-block:: shell

    $ workflow-nodes plot

The information on how to run a certain tool can be accessed via:

.. code-block:: shell

    $ workflow-nodes plot plot-matplotlib --help


.. toctree::
    :maxdepth: 3

    cli/tools
